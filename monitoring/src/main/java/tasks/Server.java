package tasks;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private final int MAX_BUFFER_SIZE = 1024 * 1024;
    private final byte[] byteBuffer = new byte[MAX_BUFFER_SIZE];
    private final int port;

    private DataOutputStream os;
    private DataInputStream is;

    private int big = 0;
    private int small = 0;

    public Server(int port) {
        this.port = port;
    }

    public void start() throws IOException {
        ServerSocket server = new ServerSocket(port);
        Socket socket = server.accept();
        os = new DataOutputStream(socket.getOutputStream());
        is = new DataInputStream(socket.getInputStream());

        while (true) {
            readMessages();
        }
    }

    private void readMessages() throws IOException {
        int numberOfMessages = is.readInt();
        for (int index = 0; index < numberOfMessages; index++) {
            int messageLength = is.readInt();

            big = 0;
            small = 0;

            int processedLength = 0;
            int chunkSize = 0;
            int processedOffset = 0;

            while (processedLength != messageLength) {
                // Shift buffer if necessary
                int previouslyUnprocessedLength = chunkSize - processedOffset;
                if (previouslyUnprocessedLength != 0) {
                    System.arraycopy(byteBuffer, processedOffset, byteBuffer, 0, previouslyUnprocessedLength);
                }

                // read next chunk
                int unreceivedLength = messageLength - processedLength;
                int canReadLength = MAX_BUFFER_SIZE - previouslyUnprocessedLength;
                if (unreceivedLength > canReadLength) {
                    chunkSize = canReadLength;
                } else {
                    chunkSize = unreceivedLength;
                }
                processedLength += chunkSize;
                is.readFully(byteBuffer, previouslyUnprocessedLength, chunkSize);

                // Calculate partial results
                processedOffset = calcSentence(byteBuffer, chunkSize);
            }
            // do last calculations
            if (processedOffset != chunkSize) {
                if (byteBuffer[processedOffset] != ' ') {
                    if (chunkSize - processedOffset <= 4) {
                        ++small;
                    } else {
                        ++big;
                    }
                }
            }

            // write answer
            try {
                os.writeInt(index);
                os.writeInt(big);
                os.writeInt(small);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int findWordBegin(byte[] bytes, int begin, int length) {
        int offset = begin;
        for (; offset < length; ++offset) {
            if (bytes[offset] != ' ') {
                break;
            }
        }
        return offset;
    }

    private int findWordEnd(byte[] bytes, int begin, int length) {
        int offset = begin;
        for (; offset < length; ++offset) {
            if (bytes[offset] == ' ') {
                break;
            }
        }
        return offset;
    }

    private int calcSentence(byte[] bytes, int length) {
        int offset = 0;
        int beginWord = 0;
        int endWord = 0;
        while (offset < length) {
            offset = findWordBegin(bytes, offset, length);
            beginWord = offset;
            if (beginWord == length) {
                return endWord;
            }

            offset = findWordEnd(bytes, offset, length);
            endWord = offset;
            if (endWord == length) {
                return beginWord;
            }

            if (endWord - beginWord <= 4) {
                ++small;
            } else {
                ++big;
            }
        }
        return offset;
    }

    public static void main(String[] args) throws IOException {
        new Server(7890).start();
    }
}