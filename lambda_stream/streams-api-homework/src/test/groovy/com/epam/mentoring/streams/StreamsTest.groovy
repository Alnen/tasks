package com.epam.mentoring.streams

import com.epam.mentoring.streams.domain.Account
import com.epam.mentoring.streams.domain.Currency
import com.epam.mentoring.streams.domain.TotalAmount
import org.junit.Test

import static com.epam.mentoring.streams.Streams.collectToUniqueValues
import static com.epam.mentoring.streams.Streams.findMaxAccountByCurrency
import static com.epam.mentoring.streams.Streams.groupAndCountTotalAmountsByCurrenciesSkippingCurrencies
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

class StreamsTest {

    private static final List<Account> ACCOUNTS = [
            new Account("00000000000000000000", 100000, new Currency("RUR", 100)),
            new Account("00000000000000000001", 200000, new Currency("RUR", 100)),
            new Account("00000000000000000002", 300000, new Currency("EUR", 100)),
            new Account("00000000000000000003", 400000, new Currency("EUR", 100)),
            new Account("00000000000000000004", 500000, new Currency("EUR", 100)),
            new Account("00000000000000000005", 600000, new Currency("USD", 100)),
            new Account("00000000000000000006", 700000, new Currency("USD", 100)),
            new Account("00000000000000000007", 800000, new Currency("USD", 100)),
            new Account("00000000000000000008", null, new Currency("USD", 100)),
            new Account("00000000000000000009", 100000, new Currency("USD", 100)),
            new Account("00000000000000000010", 110000, new Currency("JPY", 100))
    ]

    private static final List<List<String>> STRINGS_LISTS = [
            ["one", "two", "three", "three", "three"],
            ["four", "five", "six"]
    ]

    @Test
    void thatTotalAmountsCalculatedCorrectly() {
        def actual = groupAndCountTotalAmountsByCurrenciesSkippingCurrencies(ACCOUNTS, "JPY")
        def expected = [
                (new Currency("USD", 100)): new TotalAmount(2200000, new Currency("USD", 100)),
                (new Currency("RUR", 100)): new TotalAmount(300000, new Currency("RUR", 100)),
                (new Currency("EUR", 100)): new TotalAmount(1200000, new Currency("EUR", 100)),
        ]
        assertTrue(actual == expected)
    }

    @Test
    void thatMaxAccountFoundByCurrency() {
        def accountNumber = findMaxAccountByCurrency(ACCOUNTS, "EUR")
        assertEquals("00000000000000000004", accountNumber)
    }

    @Test
    void thatTwoListsMergedIntoUniqueOne() {
        def uniqueCollection = collectToUniqueValues(STRINGS_LISTS)
        assertTrue(uniqueCollection == ["one", "two", "three", "four", "five", "six"])
    }
}
