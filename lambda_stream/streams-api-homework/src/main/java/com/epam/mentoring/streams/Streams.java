package com.epam.mentoring.streams;

import com.epam.mentoring.streams.domain.Account;
import com.epam.mentoring.streams.domain.Currency;
import com.epam.mentoring.streams.domain.TotalAmount;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;


public class Streams {

    static Collection<String> collectToUniqueValues(List<List<String>> stringCollections) {
        return stringCollections.stream()
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    static String findMaxAccountByCurrency(List<Account> accounts, String currencyCode) {
        return accounts.stream()
                .filter(account -> account.getCurrency().getCode().equals(currencyCode) && account.getBalance() != null)
                .max(Comparator.comparing(Account::getBalance))
                .map(Account::getNumber)
                .orElse(null);
    }

    static Map<Currency, TotalAmount> groupAndCountTotalAmountsByCurrenciesSkippingCurrencies(List<Account> accounts, String... currenciesToSkip) {
        List<String> skippLists = Arrays.asList(currenciesToSkip);
        return accounts.stream()
                .filter(account -> !skippLists.contains(account.getCurrency().getCode()) && account.getBalance() != null)
                .map(account -> new TotalAmount(account.getBalance(), account.getCurrency()))
                .collect(
                        Collectors.groupingBy(
                                TotalAmount::getCurrency,
                                Collectors.reducing(
                                        new TotalAmount(BigDecimal.ZERO, null),
                                        (totalAmount1, totalAmount2) ->
                                                new TotalAmount(
                                                        totalAmount1.getAmount().add(totalAmount2.getAmount()),
                                                        totalAmount1.getCurrency() != null ? totalAmount1.getCurrency() : totalAmount2.getCurrency())
                                )));
    }

}
