package com.epam.mentoring.streams.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;


@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class TotalAmount {
    private BigDecimal amount = ZERO;
    private Currency currency;
}
