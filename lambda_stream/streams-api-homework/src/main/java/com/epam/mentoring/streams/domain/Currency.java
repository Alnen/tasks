package com.epam.mentoring.streams.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@AllArgsConstructor
public class Currency {
    private final String code;
    private final int minority;
}
