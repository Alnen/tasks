package com.epam.mentoring.streams.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;


@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private String number;
    private BigDecimal balance;
    private Currency currency;
}
