package tasks.lambda;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

public class ApplicationTest {

    private static Author[] authors = {
            new Author("A", (short) 10),
            new Author("B", (short) 11),
            new Author("C", (short) 12),
            new Author("D", (short) 13),
            new Author("E", (short) 14)
    };
    private static Book[] books = {
            new Book("A", 10),
            new Book("B", 220),
            new Book("C", 12),
            new Book("D", 210),
            new Book("E", 14),
            new Book("X", 300),
            new Book("Z", 16),
    };

    @BeforeClass
    public static void globalSetUp() {
        Random intRandom = new Random();
        for (Author author : authors) {
            for (Book book : books) {
                if (intRandom.nextDouble() < 0.5) {
                    author.getBooks().add(book);
                    book.getAuthors().add(author);
                }
            }
        }
    }

    public boolean task1() {
        for (Book book : books) {
            if (book.getNumberOfPages() >= 200) {
                return false;
            }
        }
        return true;
    }

    public boolean task2() {
        for (Book book : books) {
            if (book.getNumberOfPages() < 200) {
                return true;
            }
        }
        return false;
    }

    public Map<String, List<Book>> task3() {
        int maxLength = Integer.MIN_VALUE;
        List<Book> maxLengthBooks = new ArrayList<>();
        int minLength = Integer.MAX_VALUE;
        List<Book> minLengthBooks = new ArrayList<>();

        for (Book book : books) {
            int numberOfPages = book.getNumberOfPages();

            if (numberOfPages > maxLength) {
                maxLength = numberOfPages;
                maxLengthBooks.clear();
            }

            if (numberOfPages == maxLength) {
                maxLengthBooks.add(book);
            }

            if (numberOfPages < minLength) {
                minLength = numberOfPages;
                minLengthBooks.clear();
            }

            if (numberOfPages == minLength) {
                minLengthBooks.add(book);
            }
        }

        Map<String, List<Book>> answer = new HashMap<>();
        answer.put("max", maxLengthBooks);
        answer.put("min", minLengthBooks);
        return answer;
    }

    public List<Book> task4() {
        List<Book> answer = new ArrayList<>();
        for (Book book : books) {
            if (book.getAuthors().size() == 1) {
                answer.add(book);
            }
        }
        return answer;
    }

    public List<Book> task5() {
        List<Book> sortedBooks = Arrays.asList(books);
        sortedBooks.sort(Comparator.comparingInt(Book::getNumberOfPages).thenComparing(Book::getTitle));
        return sortedBooks;
    }

    public Set<String> task6() {
        Set<String> titles = new HashSet<>();
        for (Book book : books) {
            titles.add(book.getTitle());
        }
        return titles;
    }

    public Set<Author> task7() {
        Set<Author> distAuthors = new HashSet<>();
        distAuthors.addAll(Arrays.asList(authors));
        return distAuthors;
    }

    public Optional<Book> getLargestBookByAuthor(String name) {
        for (Author author : authors) {
            if (Objects.equals(author.getName(), name)) {
                int largeBookLength = Integer.MIN_VALUE;
                Book largestBook = null;

                for (Book book : books) {
                    if (book.getNumberOfPages() > largeBookLength) {
                        largeBookLength = book.getNumberOfPages();
                        largestBook = book;
                    }
                }

                return Optional.ofNullable(largestBook);
            }
        }

        return Optional.empty();
    }

    @Test
    public void test_sequential_1() {
//        check if some/all book(s) have more than 200 pages;
        boolean task1Actual = Arrays.stream(books).allMatch(book -> book.getNumberOfPages() < 200);
        Assert.assertEquals(task1(), task1Actual);
    }
    @Test
    public void test_sequential_2() {
        boolean task2Actual = Arrays.stream(books).anyMatch(book -> book.getNumberOfPages() < 200);
        Assert.assertEquals(task2(), task2Actual);
    }
    @Test
    public void test_sequential_3() {
//        find the books with max and min number of pages;
        IntSummaryStatistics stat = Arrays.stream(books)
                .mapToInt(Book::getNumberOfPages)
                .summaryStatistics();
        Map<String, List<Book>> booksWithMinMaxPagex = Arrays.stream(books)
                .filter(b -> b.getNumberOfPages() == stat.getMin() || b.getNumberOfPages() == stat.getMax())
                .collect(Collectors.groupingBy(b -> (b.getNumberOfPages() == stat.getMax()) ? "max" : "min"));
        Assert.assertEquals(task3(), booksWithMinMaxPagex);
    }
    @Test
    public void test_sequential_4() {
//        filter books with only single author;
        List<Book> booksWithSingleAuthor = Arrays.stream(ApplicationTest.books)
                .filter(b -> b.getAuthors().size() == 1)
                .collect(Collectors.toList());
        Assert.assertEquals(task4(), booksWithSingleAuthor);
    }
    @Test
    public void test_sequential_5() {
//        sort the books by number of pages/title;
        List<Book> sortedBooks = Arrays.stream(ApplicationTest.books)
                .sorted(Comparator.comparingInt(Book::getNumberOfPages).thenComparing(Book::getTitle))
                .collect(Collectors.toList());
        Assert.assertEquals(task5(), sortedBooks);
    }
    @Test
    public void test_sequential_6() {
//        get list of all titles;
        Set<String> titles = Arrays.stream(ApplicationTest.books)
                .map(Book::getTitle)
                .distinct()
                .collect(Collectors.toSet());
        Assert.assertEquals(task6(), titles);
        //        print them using forEach method;
        titles.forEach(System.out::println);
    }
    @Test
    public void test_sequential_7() {
//        get distinct list of all authors
        Set<Author> distAuthors = Arrays.stream(authors).distinct().collect(Collectors.toSet());
        Assert.assertEquals(task7(), distAuthors);
    }

    @Test
    public void test_sequential_8() {
//        Use the Option type for determining the title of the biggest book of some author.
        String queryName = "name";
        Optional<Book> biggestBook = Arrays.stream(authors)
                .filter(author -> author.getName() == queryName)
                .flatMap(author -> author.getBooks().stream())
                .collect(Collectors.maxBy(Comparator.comparingInt(Book::getNumberOfPages)));
        Assert.assertEquals(getLargestBookByAuthor(queryName), biggestBook);
    }

    @Test
    public void test_parallel_1() {
//        check if some/all book(s) have more than 200 pages;
        boolean task1Actual = Arrays.stream(books).parallel().allMatch(book -> book.getNumberOfPages() < 200);
        Assert.assertEquals(task1(), task1Actual);
    }
    @Test
    public void test_parallel_2() {
        boolean task2Actual = Arrays.stream(books).parallel().anyMatch(book -> book.getNumberOfPages() < 200);
        Assert.assertEquals(task2(), task2Actual);
    }
    @Test
    public void test_parallel_3() {
//        find the books with max and min number of pages;
        IntSummaryStatistics stat = Arrays.stream(books).parallel()
                .mapToInt(Book::getNumberOfPages)
                .summaryStatistics();
        Map<String, List<Book>> booksWithMinMaxPagex = Arrays.stream(books).parallel()
                .filter(b -> b.getNumberOfPages() == stat.getMin() || b.getNumberOfPages() == stat.getMax())
                .collect(Collectors.groupingByConcurrent(b -> (b.getNumberOfPages() == stat.getMax()) ? "max" : "min"));
        Assert.assertEquals(task3(), booksWithMinMaxPagex);
    }
    @Test
    public void test_parallel_4() {
//        filter books with only single author;
        List<Book> booksWithSingleAuthor = Arrays.stream(ApplicationTest.books).parallel()
                .filter(b -> b.getAuthors().size() == 1)
                .collect(Collectors.toList());
        Assert.assertEquals(task4(), booksWithSingleAuthor);
    }
    @Test
    public void test_parallel_5() {
//        sort the books by number of pages/title;
        List<Book> sortedBooks = Arrays.stream(ApplicationTest.books).parallel()
                .sorted(Comparator.comparingInt(Book::getNumberOfPages).thenComparing(Book::getTitle))
                .collect(Collectors.toList());
        Assert.assertEquals(task5(), sortedBooks);
    }
    @Test
    public void test_parallel_6() {
//        get list of all titles;
        Set<String> titles = Arrays.stream(ApplicationTest.books).parallel()
                .map(Book::getTitle)
                .distinct()
                .collect(Collectors.toSet());
        Assert.assertEquals(task6(), titles);
        //        print them using forEach method;
        titles.forEach(System.out::println);
    }
    @Test
    public void test_parallel_7() {
//        get distinct list of all authors
        Set<Author> distAuthors = Arrays.stream(authors).parallel().distinct().collect(Collectors.toSet());
        Assert.assertEquals(task7(), distAuthors);
    }

    @Test
    public void test_parallel_8() {
//        Use the Option type for determining the title of the biggest book of some author.
        String queryName = "name";
        Optional<Book> biggestBook = Arrays.stream(authors).parallel()
                .filter(author -> author.getName() == queryName)
                .flatMap(author -> author.getBooks().stream())
                .collect(Collectors.maxBy(Comparator.comparingInt(Book::getNumberOfPages)));
        Assert.assertEquals(getLargestBookByAuthor(queryName), biggestBook);
    }
}