package tasks.restserver.service;


import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import java.io.InputStream;
import java.nio.file.Path;

public interface LogoService {

    public Path saveLogo(String login, InputStream uploadedInputStream, FormDataContentDisposition fileDetail);

    public Path getLogo(String login);

    public Path deleteLogo(String login);
}
