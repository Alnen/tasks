package tasks.restserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tasks.restserver.repository.UserDirectoryRepository;

import java.util.List;
import java.util.Map;

@Service
public class UserDirectoryServiceImpl implements UserDirectoryService {

    private UserDirectoryRepository userDirectoryRepository;

    @Autowired
    public void setUserDirectoryRepository(UserDirectoryRepository userDirectoryRepository) {
        this.userDirectoryRepository = userDirectoryRepository;
    }

    @Override
    public List<User> getUsers() {
        return userDirectoryRepository.getUsers();
    }

    @Override
    public User getUser(String login) {
        return userDirectoryRepository.getUser(login);
    }

    @Override
    public User createUser(String login, String firstName, String lastName, String email) {
        User user = new User(login, firstName, lastName, email);
        
        try {
            userDirectoryRepository.createUser(user);
        } catch (RuntimeException ignored) {
            user = null;
        }

        return user;
    }

    @Override
    public User updateUser(String login, Map<String, String> updates) {
        return userDirectoryRepository.updateUser(login, updates);
    }

    @Override
    public User deleteUser(String login) {
        return userDirectoryRepository.deleteUser(login);
    }
}
