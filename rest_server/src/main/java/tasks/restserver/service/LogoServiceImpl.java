package tasks.restserver.service;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tasks.restserver.repository.ImageRepository;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class LogoServiceImpl implements LogoService {

    private Path basePath = Files.createTempDirectory("logoImages");
    private ImageRepository imageRepository;
    private UserDirectoryService userDirectoryService;

    public LogoServiceImpl() throws IOException {
    }

    @Autowired
    public void setImageRepository(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Autowired
    public void setUserDirectoryService(UserDirectoryService userDirectoryService) {
        this.userDirectoryService = userDirectoryService;
    }

    private boolean isUserExists(String login) {
        return userDirectoryService.getUser(login) != null;
    }

    @Override
    public Path saveLogo(String login, InputStream uploadedInputStream, FormDataContentDisposition fileDetail) {
        if (!isUserExists(login)) {
            return null;
        }

        Path imagePath = Paths.get(basePath.toString(), fileDetail.getFileName());
        try {
            writeToFile(uploadedInputStream, imagePath);
            imageRepository.saveImage(login, imagePath);
        } catch (IOException ignored) {
            imagePath = null;
        }
        return imagePath;
    }

    @Override
    public Path getLogo(String login) {
        return imageRepository.getImage(login);
    }

    @Override
    public Path deleteLogo(String login) {
        return imageRepository.deleteImage(login);
    }

    private void writeToFile(InputStream uploadedInputStream, Path imagePath) throws IOException {
        try (OutputStream out = Files.newOutputStream(imagePath)) {
            int read = 0;
            byte[] buffer = new byte[1024];

            while ((read = uploadedInputStream.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
        }
    }
}
