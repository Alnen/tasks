package tasks.restserver.service;

import java.util.List;
import java.util.Map;

public interface UserDirectoryService {

    public List<User> getUsers();

    public User getUser(String login);

    public User createUser(String login, String firstName, String lastName, String email);

    public User updateUser(String login, Map<String, String> updates);

    public User deleteUser(String login);
}
