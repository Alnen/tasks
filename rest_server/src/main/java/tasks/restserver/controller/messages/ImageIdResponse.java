package tasks.restserver.controller.messages;

import java.nio.file.Path;

public class ImageIdResponse {

    private String imageId;

    public ImageIdResponse() {
    }

    public ImageIdResponse(String imageId) {
        this.imageId = imageId;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }
}
