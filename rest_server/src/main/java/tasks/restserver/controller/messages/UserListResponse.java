package tasks.restserver.controller.messages;

import tasks.restserver.service.User;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "users")
public class UserListResponse {

    private List<User> user;

    public UserListResponse() {
    }

    @XmlElement(name = "user")
    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }
}
