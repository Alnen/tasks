package tasks.restserver.controller.messages;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
public class NewUserRequest {

    private String login;
    private String lastName;
    private String firstName;
    private String email;

    public NewUserRequest() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
