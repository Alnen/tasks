package tasks.restserver.controller.messages;

import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.Map;

public class UserUpdatePropertiesRequest {

    private Map<String, String> updates = new HashMap<>();

    public Map<String, String> getUpdates() {
        return updates;
    }

    @JsonAnySetter
    public void addUpdate(String name, String value) {
        updates.put(name, value);
    }

}
