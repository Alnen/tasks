package tasks.restserver.controller;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tasks.restserver.controller.messages.ImageIdResponse;
import tasks.restserver.service.LogoService;

import javax.ws.rs.*;
import javax.ws.rs.Path;
import javax.ws.rs.core.*;
import java.io.*;
import java.nio.file.*;


@Component
@Path("/logo")
public class LogoController {

    private LogoService logoService;

    @Autowired
    public void setLogoService(LogoService logoService) {
        this.logoService = logoService;
    }

    @POST
    @Path("/{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadLogo(@PathParam("id") String login,
                               @FormDataParam("file") InputStream uploadedInputStream,
                               @FormDataParam("file") FormDataContentDisposition fileDetail,
                               @Context UriInfo uriInfo) {
        java.nio.file.Path imagePath = logoService.saveLogo(login, uploadedInputStream, fileDetail);

        if (imagePath != null) {
            UriBuilder builder = uriInfo.getAbsolutePathBuilder();
            builder.path(login);

            return Response.status(Response.Status.OK)
                    .entity(new ImageIdResponse(imagePath.toString()))
                    .location(builder.build())
                    .build();
        } else {
            return Response.status(Response.Status.CONFLICT)
                    .build();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadLogo(@PathParam("id") String login) {
        java.nio.file.Path logoPath = logoService.getLogo(login);
        if (logoPath == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .build();
        }

        StreamingOutput fileStream = new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                try {
                    byte[] data = Files.readAllBytes(logoPath);
                    output.write(data);
                    output.flush();
                } catch (Exception e) {
                    throw new RuntimeException("Error while reading file");
                }
            }
        };

        return Response
                .status(Response.Status.OK)
                .entity(fileStream)
                .header("content-disposition", "attachment; filename = " + logoPath.getFileName())
                .build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteLogo(@PathParam("id") String login) {
        java.nio.file.Path path = logoService.deleteLogo(login);

        if (path != null) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

    }
}
