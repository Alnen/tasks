package tasks.restserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import tasks.restserver.controller.messages.NewUserRequest;
import tasks.restserver.controller.messages.UserListResponse;
import tasks.restserver.service.User;
import tasks.restserver.controller.messages.UserUpdatePropertiesRequest;
import tasks.restserver.service.UserDirectoryService;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;

@Controller
@Path("/users")
public class UserDirectoryController {

    private UserDirectoryService userDirectoryService;

    @Autowired
    public void setUserDirectoryService(UserDirectoryService userDirectoryService) {
        this.userDirectoryService = userDirectoryService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response createUser(NewUserRequest newUserRequest, @Context UriInfo uriInfo) {
        User user = userDirectoryService.createUser(
                newUserRequest.getLogin(),
                newUserRequest.getFirstName(),
                newUserRequest.getLastName(),
                newUserRequest.getEmail());

        if (user != null) {
            UriBuilder builder = uriInfo.getAbsolutePathBuilder();
            builder.path(user.getLogin());

            return Response.status(Response.Status.CREATED)
                    .location(builder.build())
                    .entity(user)
                    .build();
        } else {
            return Response.status(Response.Status.CONFLICT)
                    .build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getUsers() {
        List<User> users = userDirectoryService.getUsers();
        UserListResponse userListResponse = new UserListResponse();
        userListResponse.setUser(users);
        return Response.status(Response.Status.OK)
                .entity(userListResponse)
                .build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_XML)
    public Response getUser(@PathParam("id") String login) {
        User user = userDirectoryService.getUser(login);
        if (user == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .build();

        }

        return Response.status(Response.Status.OK).entity(user).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("id") String login, UserUpdatePropertiesRequest userUpdatePropertiesRequest) {
        User newUser = userDirectoryService.updateUser(login, userUpdatePropertiesRequest.getUpdates());

        if (newUser != null) {
            return Response.status(Response.Status.OK)
                    .entity(newUser)
                    .build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .build();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response deleteUser(@PathParam("id") String login) {
        User oldUser = userDirectoryService.deleteUser(login);
        if (oldUser != null) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}