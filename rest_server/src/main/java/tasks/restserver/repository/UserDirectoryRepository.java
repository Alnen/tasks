package tasks.restserver.repository;

import tasks.restserver.service.User;

import java.util.List;
import java.util.Map;

public interface UserDirectoryRepository {

    List<User> getUsers();
    User getUser(String login);

    void createUser(User user);
    User updateUser(String login, Map<String, String> userPropertyUpdates);
    User updateUser(String login, String propertyName, String propertyValue);
    User deleteUser(String login);
}
