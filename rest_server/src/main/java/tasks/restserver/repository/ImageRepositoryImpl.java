package tasks.restserver.repository;

import org.springframework.stereotype.Repository;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ImageRepositoryImpl implements ImageRepository {

    private Map<String, Path> imageRepository = new HashMap<>();

    @Override
    public void saveImage(String imageId, Path image) {
        imageRepository.put(imageId, image);
    }

    @Override
    public Path getImage(String imageId) {
        return imageRepository.get(imageId);
    }

    @Override
    public Path deleteImage(String imageId) {
        return imageRepository.remove(imageId);
    }
}
