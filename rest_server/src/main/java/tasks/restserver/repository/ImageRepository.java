package tasks.restserver.repository;

import java.nio.file.Path;

public interface ImageRepository {

    void saveImage(String imageId, Path image);

    Path getImage(String imageId);

    Path deleteImage(String imageId);
}
