package tasks.restserver.repository;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import tasks.restserver.service.User;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class UserDirectoryRepositoryImpl implements UserDirectoryRepository {

    private Map<String, User> userRepository = new HashMap<>();

    @Override
    public synchronized List<User> getUsers() {
        return userRepository
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    @Override
    public synchronized User getUser(String login) {
        return userRepository.get(login);
    }

    @Override
    public synchronized void createUser(User user) {
        String login = user.getLogin();
        User storedUser = userRepository.get(login);
        if (storedUser != null) {
            throw new RuntimeException("User with login " + login + " already exists");
        }

        userRepository.put(user.getLogin(), user);
    }

    @Override
    public synchronized User updateUser(String login, Map<String, String> userPropertyUpdates) {
        User user = userRepository.get(login);

        if (user != null) {
            for (Map.Entry<String, String> update : userPropertyUpdates.entrySet()) {
                updateProperty(user, update.getKey(), update.getValue());
            }
        }

        return user;
    }

    @Override
    public User updateUser(String login, String propertyName, String propertyValue) {
        User user = userRepository.get(login);
        if (user != null) {
            updateProperty(user, propertyName, propertyValue);
        }
        return user;
    }

    private void updateProperty(User user, String propertyName, String propertyValue) {
        switch (propertyName) {
            case "firstName":
                user.setFirstName(propertyValue);
                break;
            case "lastName":
                user.setLastName(propertyValue);
                break;
            case "email":
                user.setEmail(propertyValue);
                break;
            default:
                throw new RuntimeException("Unknown field " + propertyValue);
        }
    }

    @Override
    public synchronized User deleteUser(String login) {
        return userRepository.remove(login);
    }
}
