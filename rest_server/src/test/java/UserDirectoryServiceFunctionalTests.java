import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import tasks.restserver.service.User;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserDirectoryServiceFunctionalTests {

    private static final String TEST_LOGIN = "testLogin";
    private static final String TEST_FIRST_NAME = "testFirstName";
    private static final String TEST_LAST_NAME = "testLastName";
    private static final String TEST_EMAIL = "testEmail";
    private static final User TEST_USER = new User(TEST_LOGIN, TEST_FIRST_NAME, TEST_LAST_NAME, TEST_EMAIL);

    private Client client = ClientBuilder.newClient();
    private WebTarget usersTarget = client.target("http://localhost:8080").path("users");
    private WebTarget logoTarget = client.target("http://localhost:8080").path("logo");

    @Test
    public void globalSetup() {
        usersTarget.path(TEST_USER.getLogin())
                .request()
                .buildDelete()
                .invoke();
    }

    @Test
    public void test_0_createUserShouldWork() {
        Response response = usersTarget.request()
                .accept(MediaType.APPLICATION_XML)
                .buildPost(Entity.entity(TEST_USER, MediaType.APPLICATION_XML_TYPE))
                .invoke();

        Assert.assertEquals(Response.Status.CREATED, response.getStatusInfo());
        Assert.assertEquals(MediaType.APPLICATION_XML_TYPE, response.getMediaType());
        Assert.assertEquals(TEST_USER, response.readEntity(User.class));
    }

    @Test
    public void test_1_insertionOfExistingUserIsAnError() {
        Response response = usersTarget.request()
                .accept(MediaType.APPLICATION_XML)
                .buildPost(Entity.entity(TEST_USER, MediaType.APPLICATION_XML_TYPE))
                .invoke();

        Assert.assertEquals(Response.Status.CONFLICT, response.getStatusInfo());
    }

    @Test
    public void test_2_updateOfUserPropertiesShouldWork() throws IOException {
        String newTestFirstName = "newTestFirstName";

        Map<String, String> updates = new HashMap<>();
        updates.put("firstName", newTestFirstName);

        Response response = usersTarget.path(TEST_LOGIN)
                .request()
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .buildPut(Entity.entity(updates, MediaType.APPLICATION_JSON_TYPE))
                .invoke();

        User expectedUpdatedUser = new User(TEST_LOGIN, newTestFirstName, TEST_LAST_NAME, TEST_EMAIL);

        Assert.assertEquals(Response.Status.OK, response.getStatusInfo());
        Assert.assertEquals(MediaType.APPLICATION_JSON_TYPE, response.getMediaType());
        Assert.assertEquals(expectedUpdatedUser, response.readEntity(User.class));
    }

    @Test
    public void test_3_userDeletionShouldWork() throws IOException {
        Response response = usersTarget.path(TEST_LOGIN)
                .request()
                .buildDelete()
                .invoke();

        Assert.assertEquals(Response.Status.OK, response.getStatusInfo());

        response = usersTarget.path(TEST_LOGIN)
                .request()
                .buildGet()
                .invoke();

        Assert.assertEquals(Response.Status.NOT_FOUND, response.getStatusInfo());
    }
}
