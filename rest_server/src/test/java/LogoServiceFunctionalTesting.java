import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import tasks.restserver.service.User;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LogoServiceFunctionalTesting {

    private static final String TEST_LOGIN = "testLogin";
    private static final String TEST_FIRST_NAME = "testFirstName";
    private static final String TEST_LAST_NAME = "testLastName";
    private static final String TEST_EMAIL = "testEmail";
    private static final User TEST_USER = new User(TEST_LOGIN, TEST_FIRST_NAME, TEST_LAST_NAME, TEST_EMAIL);

    private Client client = ClientBuilder.newBuilder()
            .register(MultiPartFeature.class)
            .build();

    private WebTarget usersTarget = client.target("http://localhost:8080").path("users");
    private WebTarget logoTarget = client.target("http://localhost:8080").path("logo");

    @Test
    public void globalSetup() {
        usersTarget.request()
                .accept(MediaType.APPLICATION_XML)
                .buildPost(Entity.entity(TEST_USER, MediaType.APPLICATION_XML_TYPE))
                .invoke();
    }

    @Test
    public void test_0_logoUploadShouldWork() {
        InputStream stream = getClass().getClassLoader().getResourceAsStream("logo.png");
        FormDataBodyPart formDataBodyPart = new FormDataBodyPart("file",
                stream,
                MediaType.TEXT_PLAIN_TYPE);

        MultiPart multiPartEntity = new MultiPart(MediaType.MULTIPART_FORM_DATA_TYPE)
                .bodyPart(formDataBodyPart);

        Response post = logoTarget.path(TEST_LOGIN)
                .request()
                .post(Entity.entity(multiPartEntity, MediaType.MULTIPART_FORM_DATA_TYPE));

        Assert.assertEquals(Response.Status.CREATED, post.getStatusInfo());
    }
}
