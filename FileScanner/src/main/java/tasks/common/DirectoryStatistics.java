package tasks.common;

public class DirectoryStatistics {

    private long fileCount;
    private long directoryCount;
    private long totalFilesSize;

    public DirectoryStatistics() {
        this(0, 0, 0);
    }

    public DirectoryStatistics(long fileCount, long directoryCount, long totalFilesSize) {
        this.fileCount = fileCount;
        this.directoryCount = directoryCount;
        this.totalFilesSize = totalFilesSize;
    }

    public long getFileCount() {
        return fileCount;
    }

    public long getDirectoryCount() {
        return directoryCount;
    }

    public long getTotalFilesSize() {
        return totalFilesSize;
    }

    public void incrementFileCount(long fileCount) {
        this.fileCount += fileCount;
    }

    public void incrementDirectoryCount(long directoryCount) {
        this.directoryCount += directoryCount;
    }

    public void incrementTotalFilesSize(long fileSize) {
        totalFilesSize += fileSize;
    }

    public DirectoryStatistics merge(DirectoryStatistics otherStatistics) {
        return new DirectoryStatistics(
                this.fileCount + otherStatistics.fileCount,
                this.directoryCount + otherStatistics.directoryCount,
                this.totalFilesSize + otherStatistics.totalFilesSize
        );
    }

    @Override
    public String toString() {
        return "DirectoryStatistics{" +
                "fileCount=" + fileCount +
                ", directoryCount=" + directoryCount +
                ", totalFilesSize=" + totalFilesSize +
                '}';
    }
}
