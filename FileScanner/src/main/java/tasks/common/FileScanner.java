package tasks.common;

import java.io.File;

public interface FileScanner {
    DirectoryStatistics scan(File path);
}
