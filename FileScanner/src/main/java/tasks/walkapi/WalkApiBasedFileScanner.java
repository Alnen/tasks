package tasks.walkapi;

import tasks.common.DirectoryStatistics;
import tasks.common.FileScanner;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class WalkApiBasedFileScanner implements FileScanner {

    @Override
    public DirectoryStatistics scan(File path) {
        try {
            return Files.walk(Paths.get(path.toURI()))
                    .parallel()
                    .map(Path::toFile)
                    .map(file -> file.isFile() ? new DirectoryStatistics(1, 0, file.length()) : new DirectoryStatistics(0, 1, 0))
                    .reduce(new DirectoryStatistics(0, 0, 0), DirectoryStatistics::merge);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new DirectoryStatistics(0, 0, 0);
    }
}
