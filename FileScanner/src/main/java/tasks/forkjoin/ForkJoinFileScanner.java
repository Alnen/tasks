package tasks.forkjoin;

import tasks.common.DirectoryStatistics;
import tasks.common.FileScanner;

import java.io.File;
import java.util.concurrent.ForkJoinPool;

public class ForkJoinFileScanner implements FileScanner {

    private final int numberOfThreads;

    public ForkJoinFileScanner(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    @Override
    public DirectoryStatistics scan(File path) {
        ForkJoinPool pool = new ForkJoinPool(numberOfThreads);
        return pool.invoke(new tasks.forkjoin.ForkJoinTask(path));
    }
}
