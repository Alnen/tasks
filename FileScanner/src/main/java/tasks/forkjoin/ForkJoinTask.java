package tasks.forkjoin;

import tasks.common.DirectoryStatistics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.RecursiveTask;

public class ForkJoinTask extends RecursiveTask<DirectoryStatistics> {

    private File targetDirectory;

    public ForkJoinTask(File targetDirectory) {
        this.targetDirectory = targetDirectory;
    }

    @Override
    protected DirectoryStatistics compute() {
        DirectoryStatistics statistics = new DirectoryStatistics();
        if (targetDirectory.exists()) {
            List<ForkJoinTask> subTasks = new ArrayList<>();

            if (targetDirectory.isDirectory()) {
                for (File file : targetDirectory.listFiles()) {
                    try {
                        boolean isSymlink = !Objects.equals(file.getCanonicalPath(), file.getAbsolutePath());
                        if (file.isDirectory()) {
                            statistics.incrementDirectoryCount(1);
                            if (!isSymlink) {
                                ForkJoinTask task = new ForkJoinTask(file);
                                task.fork();
                                subTasks.add(task);
                            }
                        } else if (file.isFile()) {
                            statistics.incrementFileCount(1);
                            statistics.incrementTotalFilesSize(file.length());
                        }
                    } catch (IOException e) {
                        // TODO: add logging
                    }
                }

                for (ForkJoinTask task : subTasks) {
                    statistics = statistics.merge(task.join());
                }
            } else if (targetDirectory.isFile()) {
                statistics.incrementFileCount(1);
                statistics.incrementTotalFilesSize(targetDirectory.length());
            }
        }

        return statistics;
    }
}
