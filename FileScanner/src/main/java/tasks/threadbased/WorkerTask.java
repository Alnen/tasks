package tasks.threadbased;

import java.io.File;
import java.util.Arrays;

public class WorkerTask {
    private File[] filesToAnalyse;
    private boolean finished = false;

    public WorkerTask(File... filesToAnalyse) {
        this.filesToAnalyse = filesToAnalyse;
    }

    public WorkerTask(boolean finished) {
        this.finished = finished;
    }

    public File[] getFilesToAnalyse() {
        return filesToAnalyse;
    }

    public boolean isFinished() {
        return finished;
    }

    @Override
    public String toString() {
        return "WorkerTask{" +
                "filesToAnalyse=" + Arrays.toString(filesToAnalyse) +
                ", finished=" + finished +
                '}';
    }
}
