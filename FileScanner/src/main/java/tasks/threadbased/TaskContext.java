package tasks.threadbased;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskContext {

    private static final Logger LOG = LogManager.getLogger(TaskContext.class);

    private final int totalThreads;
    private Queue<WorkerTask> tasks = new ArrayDeque<>();
    private AtomicInteger waitingThreads = new AtomicInteger(0);

    public TaskContext(int totalThreads) {
        this.totalThreads = totalThreads;
    }

    public synchronized boolean addTask(WorkerTask newTask) {
        boolean result = tasks.add(newTask);
        this.notify();
        return result;
    }

    public synchronized boolean addTasks(Collection<? extends WorkerTask> newTasks) {
        boolean result = tasks.addAll(newTasks);
        this.notifyAll();
        return result;
    }

    public synchronized WorkerTask waitTask() throws InterruptedException {
        WorkerTask newTask;

        int currentWaitingThreads = waitingThreads.incrementAndGet();
        LOG.debug(String.format("%d threads sleeping out of %d", currentWaitingThreads, totalThreads));
        if (currentWaitingThreads == totalThreads && tasks.isEmpty()) {
            newTask = new WorkerTask(true);
            for (int i = 0; i < currentWaitingThreads - 1; ++i) {
                addTask(newTask);
            }
        } else {
            while (tasks.isEmpty()) {
                wait();
            }
            newTask = tasks.poll();
            currentWaitingThreads = waitingThreads.decrementAndGet();
            LOG.debug(String.format("Thread woke up. %d threads sleeping out of %d", currentWaitingThreads, totalThreads));
        }
        return newTask;
    }
}
