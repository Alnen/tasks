package tasks.threadbased;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tasks.common.DirectoryStatistics;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WorkerRunnable implements Runnable {

    private static final Logger LOG = LogManager.getLogger(WorkerRunnable.class);

    private List<WorkerTask> newTasks = new ArrayList<>();
    private TaskContext taskContext;

    private DirectoryStatistics statistics;

    public WorkerRunnable(TaskContext taskContext) {
        this.taskContext = taskContext;
    }

    @Override
    public void run() {
        statistics = new DirectoryStatistics();

        try {
            WorkerTask task;
            while (!Thread.currentThread().isInterrupted()) {
                task = taskContext.waitTask();
                if (task.isFinished()) {
                    break;
                }
                LOG.debug("Took new task " + task);
                for (File file : task.getFilesToAnalyse()) {
                    processFile(file);
                }
                if (!newTasks.isEmpty()) {
                    taskContext.addTasks(newTasks);
                    LOG.debug("Added " + newTasks.size() + " tasks");
                    newTasks.clear();
                }
            }
        } catch (InterruptedException e) {
            LOG.info("Thread " + Thread.currentThread().getName() + " interrupted");
        }
    }

    private void processFile(File file) {
        try {
            boolean isSymlink = !Objects.equals(file.getCanonicalPath(), file.getAbsolutePath());
            if (file.exists()) {
                if (file.isFile()) {
                    statistics.incrementFileCount(1);
                    statistics.incrementTotalFilesSize(file.length());
                } else if (file.isDirectory()) {
                    statistics.incrementDirectoryCount(1);
                    if (!isSymlink) {
                        File[] files = file.listFiles();
                        newTasks.add(new WorkerTask(files));
                    }
                }
            }
        } catch (IOException e) {
            LOG.error("Couldn't get canonical path for " + file.toString());
        }
    }

    public DirectoryStatistics getStatistics() {
        return statistics;
    }
}
