package tasks.threadbased;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tasks.common.DirectoryStatistics;
import tasks.common.FileScanner;

import java.io.File;

public class ThreadBasedFileScanner implements FileScanner {

    private static final Logger LOG = LogManager.getLogger(ThreadBasedFileScanner.class);

    private Thread[] threadPool;
    private WorkerRunnable[] runnables;
    private TaskContext taskContext;

    public ThreadBasedFileScanner(int numberOfThreads) {
        taskContext = new TaskContext(numberOfThreads);
        this.threadPool = new Thread[numberOfThreads];
        this.runnables = new WorkerRunnable[numberOfThreads];
        for (int i = 0; i < numberOfThreads; ++i) {
            runnables[i] = new WorkerRunnable(taskContext);
            threadPool[i] = new Thread(runnables[i]);
        }
    }

    @Override
    public DirectoryStatistics scan(File path) {
        DirectoryStatistics statistics = new DirectoryStatistics();

        try {
            taskContext.addTask(new WorkerTask(path));
            for (Thread thread : threadPool) {
                thread.start();
            }

            for (Thread thread : threadPool) {
                thread.join();
            }

            for (int i = 0; i < runnables.length; ++i) {
                DirectoryStatistics additionalStatistics = runnables[i].getStatistics();
                statistics.incrementDirectoryCount(additionalStatistics.getDirectoryCount());
                statistics.incrementTotalFilesSize(additionalStatistics.getTotalFilesSize());
                statistics.incrementFileCount(additionalStatistics.getFileCount());
            }
        } catch (InterruptedException e) {
            for (Thread thread : threadPool) {
                if (thread.isAlive()) {
                    thread.interrupt();
                }
            }
            LOG.info("Thread " + Thread.currentThread().getName() + " was interrupted");
        }

        return statistics;
    }
}
