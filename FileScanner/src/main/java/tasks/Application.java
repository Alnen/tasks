package tasks;


import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tasks.common.DirectoryStatistics;
import tasks.common.FileScanner;
import tasks.forkjoin.ForkJoinFileScanner;
import tasks.threadbased.ThreadBasedFileScanner;
import tasks.walkapi.WalkApiBasedFileScanner;

import java.io.File;

public class Application {

    private static final Logger LOG = LogManager.getLogger(Application.class);
    private static final String THREADS_KEYWORD = "threads";
    private static final String TARGET_DIRECTORY_KEYWORD = "target";
    private static final String ENGINE_TYPE_KEYWORD = "engine";

    private static class ApplicationOptions {

        private int numberOfThreads;
        private int engineType;
        private String targetPath;


        public ApplicationOptions() {
        }

        public int getNumberOfThreads() {
            return numberOfThreads;
        }

        public void setNumberOfThreads(int numberOfThreads) {
            this.numberOfThreads = numberOfThreads;
        }

        public String getTargetPath() {
            return targetPath;
        }

        public void setTargetPath(String targetPath) {
            this.targetPath = targetPath;
        }

        public int getEngineType() {
            return engineType;
        }

        public void setEngineType(int engineType) {
            this.engineType = engineType;
        }

        @Override
        public String toString() {
            return "ApplicationOptions{" +
                    "numberOfThreads=" + numberOfThreads +
                    ", targetPath='" + targetPath + '\'' +
                    '}';
        }
    }

    private static ApplicationOptions parseArguments(String... args) throws ParseException {
        ApplicationOptions applicationOptions = new ApplicationOptions();

        Options options = new Options();

        Option numberOfThreadsOption = new Option(THREADS_KEYWORD, "numberOfThreads", true, "Number of threads to use");
        numberOfThreadsOption.setType(Integer.class);
        options.addOption(numberOfThreadsOption);

        Option engineTypeOption = new Option(ENGINE_TYPE_KEYWORD, "engineType", true, "Engine type to use(1 - thread based; 2 - walkapi based; 3 - forkjoin based");
        engineTypeOption.setType(Integer.class);
        options.addOption(engineTypeOption);

        Option targetDirectoryOptionOption = new Option(TARGET_DIRECTORY_KEYWORD, "targetDirectory", true, "Target directory");
        targetDirectoryOptionOption.setRequired(true);
        options.addOption(targetDirectoryOptionOption);

        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine commandLineArguments = parser.parse(options, args);

            String threadNumber = commandLineArguments.getOptionValue(THREADS_KEYWORD);
            if (threadNumber != null) {
                applicationOptions.setNumberOfThreads(Integer.valueOf(threadNumber));
            } else {
                applicationOptions.setNumberOfThreads(Runtime.getRuntime().availableProcessors());
            }

            String engineType = commandLineArguments.getOptionValue(ENGINE_TYPE_KEYWORD);
            if (engineType != null) {
                applicationOptions.setEngineType(Integer.valueOf(engineType));
            } else {
                applicationOptions.setEngineType(1);
            }

            String targetDirectory = commandLineArguments.getOptionValue(TARGET_DIRECTORY_KEYWORD);
            applicationOptions.setTargetPath(targetDirectory);

            return applicationOptions;
        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("FileScanner", options, true);
            throw e;
        }
    }

    public static void main(String[] args) throws ParseException {
        ApplicationOptions applicationOptions = parseArguments(args);

        long begin = System.currentTimeMillis();

        FileScanner scanner = null;
        switch (applicationOptions.getEngineType()) {
            case 1:
                LOG.info("Using thread based engine");
                scanner = new ThreadBasedFileScanner(applicationOptions.getNumberOfThreads());
                break;
            case 2:
                LOG.info("Using walk-api based engine");
                scanner = new WalkApiBasedFileScanner();
                break;
            case 3:
                LOG.info("Using fork-join pool based engine");
                scanner = new ForkJoinFileScanner(applicationOptions.getNumberOfThreads());
                break;
            default:
                LOG.error("Unknown engine type " + applicationOptions.getEngineType());
                System.exit(1);
        }

        File targetDirectory = new File(applicationOptions.getTargetPath());
        if (!targetDirectory.isAbsolute()) {
            targetDirectory = targetDirectory.getAbsoluteFile();
        }
        LOG.info("Gathering statistics for: " + targetDirectory.toString());
        DirectoryStatistics scan = scanner.scan(targetDirectory);

        LOG.info("Result -> " + scan);
        LOG.info("It took " + (System.currentTimeMillis() - begin) + " msec");
        System.exit(0);
    }
}
