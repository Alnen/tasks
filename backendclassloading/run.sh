#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd ${DIR}/dynamiclibrary

mvn dependency:copy-dependencies

echo "Compiling classes..."
javac -proc:none -d ${DIR}/compiled-classes \
    -cp "./src/main/java:./target/dependency/log4j-api-2.6.2.jar:./target/dependency/log4j-core-2.6.2.jar" \
    ./src/main/java/tasks/backend_classloading/Cat.java

javac -proc:none -d ${DIR}/compiled-classes \
    -cp "./src/main/java:./target/dependency/log4j-api-2.6.2.jar:./target/dependency/log4j-core-2.6.2.jar" \
    ./src/main/java/tasks/backend_classloading/Dog.java
echo "Done"


cd ${DIR}/customclassloader

mvn package

echo "Starting application..."
java -jar target/customclassloader-0.1.0.jar "${DIR}/compiled-classes"
echo "Done"

cd ${DIR}