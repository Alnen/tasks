package tasks.backend_classloading;

public interface Animal {

    void play();

    void voice();
}
