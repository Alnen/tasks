package tasks.backend_classloading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Dog implements Animal {

    private static final Logger LOG = LogManager.getLogger(Dog.class);

    public void play() {
        LOG.info("It's play method of Dog class");
    }

    public void voice() {
        LOG.info("It's voice method of Dog class");
    }
}
