package tasks.backend_classloading;

import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        CustomClassLoader classLoader = new CustomClassLoader(App.class.getClassLoader(), args[0]);
        Class<?> catClass = classLoader.loadClass("tasks.backend_classloading.Cat");
        Class<?> dogClass = classLoader.loadClass("tasks.backend_classloading.Dog");

        List<Animal> animals = new ArrayList<>();
        animals.add((Animal) catClass.newInstance());
        animals.add((Animal) dogClass.newInstance());

        for (Animal animal : animals) {
            animal.play();
            animal.voice();
        }
    }
}
