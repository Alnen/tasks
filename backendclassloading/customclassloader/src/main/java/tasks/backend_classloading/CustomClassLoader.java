package tasks.backend_classloading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CustomClassLoader extends ClassLoader {
    private static final Logger LOG = LogManager.getLogger(CustomClassLoader.class);

    private final String BASE_PATH;

    public CustomClassLoader(ClassLoader parent, String basePath) {
        super(parent);
        BASE_PATH = basePath;
    }

    public CustomClassLoader(String basePath) {
        this(CustomClassLoader.class.getClassLoader(), basePath);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        Class<?> result = findLoadedClass(name);
        if (result != null) {
            LOG.info("Class " + name + " found in cache");
            return result;
        }

        String binaryClassRelativePath = name.replace('.', File.separatorChar) + ".class";
        Path binaryClassPath = Paths.get(BASE_PATH, binaryClassRelativePath);
        if (!Files.exists(binaryClassPath)) {
            LOG.info("Class " + name + " not found in " + binaryClassPath);
            return findSystemClass(name);
        }

        try {
            byte[] classBytes = Files.readAllBytes(binaryClassPath);
            result = defineClass(name, classBytes, 0, classBytes.length);
        } catch (IOException e) {
            throw new ClassNotFoundException("Cannot load class " + name, e);
        } catch (ClassFormatError e) {
            throw new ClassNotFoundException("Format of class file incorrect for class " + name, e);
        }

        LOG.info("Successfully loaded class " + name + " from path " + binaryClassPath);

        return result;
    }
}
