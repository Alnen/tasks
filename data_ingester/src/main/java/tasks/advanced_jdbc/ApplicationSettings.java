package tasks.advanced_jdbc;

public class ApplicationSettings {

    private String uri;
    private String userName;
    private String password;

    private int numberOfTables;
    private int numberOfRows;
    private int numberOfColumns;
    private int numberOfThreads;

    private boolean batchMode;
    private boolean singleInsert;

    public ApplicationSettings(
            String uri,
            String userName,
            String password,
            int numberOfTables,
            int numberOfRows,
            int numberOfColumns,
            int numberOfThreads,
            boolean batchMode,
            boolean singleInsert) {
        this.uri = uri;
        this.userName = userName;
        this.password = password;
        this.numberOfTables = numberOfTables;
        this.numberOfRows = numberOfRows;
        this.numberOfColumns = numberOfColumns;
        this.numberOfThreads = numberOfThreads;
        this.batchMode = batchMode;
        this.singleInsert = singleInsert;
    }

    public String getUri() {
        return uri;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public int getNumberOfTables() {
        return numberOfTables;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public int getNumberOfColumns() {
        return numberOfColumns;
    }

    public int getNumberOfThreads() {
        return numberOfThreads;
    }

    public boolean isBatchMode() {
        return batchMode;
    }

    public boolean isSingleInsert() {
        return singleInsert;
    }

    @Override
    public String toString() {
        return "ApplicationSettings{" +
                "uri='" + uri + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", numberOfTables=" + numberOfTables +
                ", numberOfRows=" + numberOfRows +
                ", numberOfColumns=" + numberOfColumns +
                ", numberOfThreads=" + numberOfThreads +
                ", batchMode=" + batchMode +
                ", singleInsert=" + singleInsert +
                '}';
    }
}
