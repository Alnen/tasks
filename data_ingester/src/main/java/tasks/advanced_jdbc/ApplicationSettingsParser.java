package tasks.advanced_jdbc;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationSettingsParser {

    private static final Logger LOG = LogManager.getLogger(ApplicationSettingsParser.class);

    private static final String URI_ARGUMENT = "uri";
    private static final String USERNAME_ARGUMENT = "user";
    private static final String PASSWORD_ARGUMENT = "password";
    private static final String TABLES_COUNT_ARGUMENT = "tables";
    private static final String ROWS_COUNT_ARGUMENT = "rows";
    private static final String COLUMNS_COUNT_ARGUMENT = "columns";
    private static final String THREADS_COUNT_ARGUMENT = "threads";
    private static final String BATCH_FLAG = "batch";
    private static final String SINGLE_INSERT = "useSingleInsert";

    public static ApplicationSettings parseCommandLineArguments(String[] args) throws ParseException {
        Options options = new Options();

        options.addOption(
                Option.builder(URI_ARGUMENT)
                        .type(String.class)
                        .required()
                        .hasArg()
                        .numberOfArgs(1)
                        .desc("Database uri")
                        .build());

        options.addOption(
                Option.builder(USERNAME_ARGUMENT)
                        .type(String.class)
                        .required()
                        .hasArg()
                        .numberOfArgs(1)
                        .desc("Database username")
                        .build());

        options.addOption(
                Option.builder(PASSWORD_ARGUMENT)
                        .type(String.class)
                        .required()
                        .hasArg()
                        .numberOfArgs(1)
                        .desc("Database password")
                        .build());

        options.addOption(
                Option.builder(TABLES_COUNT_ARGUMENT)
                        .type(Integer.class)
                        .required()
                        .hasArg()
                        .numberOfArgs(1)
                        .desc("Number of tables to populate")
                        .build());

        options.addOption(
                Option.builder(ROWS_COUNT_ARGUMENT)
                        .type(Integer.class)
                        .required()
                        .hasArg()
                        .numberOfArgs(1)
                        .desc("Number of rows to populate in each table")
                        .build());

        options.addOption(
                Option.builder(COLUMNS_COUNT_ARGUMENT)
                        .type(Integer.class)
                        .required()
                        .hasArg()
                        .numberOfArgs(1)
                        .desc("Number of columns to populate in each table")
                        .build());

        options.addOption(
                Option.builder(THREADS_COUNT_ARGUMENT)
                        .type(Integer.class)
                        .required()
                        .hasArg()
                        .numberOfArgs(1)
                        .desc("Number of threads used to populate")
                        .build());

        options.addOption(
                Option.builder(BATCH_FLAG)
                        .type(Boolean.class)
                        .required(false)
                        .desc("Batch mode")
                        .build()
        );

        options.addOption(
                Option.builder(SINGLE_INSERT)
                        .type(Boolean.class)
                        .required(false)
                        .desc("Single insert mode")
                        .build()
        );


        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine commandLine = parser.parse(options, args);
            return new ApplicationSettings(
                    commandLine.getOptionValue(URI_ARGUMENT),
                    commandLine.getOptionValue(USERNAME_ARGUMENT),
                    commandLine.getOptionValue(PASSWORD_ARGUMENT),
                    Integer.valueOf(commandLine.getOptionValue(TABLES_COUNT_ARGUMENT)),
                    Integer.valueOf(commandLine.getOptionValue(ROWS_COUNT_ARGUMENT)),
                    Integer.valueOf(commandLine.getOptionValue(COLUMNS_COUNT_ARGUMENT)),
                    Integer.valueOf(commandLine.getOptionValue(THREADS_COUNT_ARGUMENT)),
                    commandLine.hasOption(BATCH_FLAG),
                    commandLine.hasOption(SINGLE_INSERT));
        } catch (ParseException e) {
            LOG.error("Could not parse arguments", e);
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("DataIngester", options, true);
            throw e;
        }
    }
}
