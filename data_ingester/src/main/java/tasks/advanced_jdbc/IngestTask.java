package tasks.advanced_jdbc;

public class IngestTask {

    private String tableName;
    private int numberOfRows;
    private int numberOfColumns;

    public IngestTask(String tableName, int numberOfRows, int numberOfColumns) {
        this.tableName = tableName;
        this.numberOfRows = numberOfRows;
        this.numberOfColumns = numberOfColumns;
    }

    public String getTableName() {
        return tableName;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public int getNumberOfColumns() {
        return numberOfColumns;
    }

    @Override
    public String toString() {
        return "IngestTask{" +
                "tableName='" + tableName + '\'' +
                ", numberOfRows='" + numberOfRows + '\'' +
                ", numberOfColumns='" + numberOfColumns + '\'' +
                '}';
    }
}
