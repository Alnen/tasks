package tasks.advanced_jdbc;


import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class App {

    private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final int ROWS_PER_TASK = 500;

    private static final Logger LOG = LogManager.getLogger(App.class);

    private static List<IngestTask> generateTasks(ApplicationSettings settings) throws SQLException {
        List<IngestTask> tasks = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(
                settings.getUri(),
                settings.getUserName(),
                settings.getPassword());
             Statement statement = connection.createStatement()) {
            for (int i = 0; i < settings.getNumberOfTables(); ++i) {
                String tableName = "table_" + UUID.randomUUID().toString().replace('-', '_');

                String createTableStatement = buildCreateTableStatement(settings, tableName);
                LOG.info("Create statement: " + createTableStatement);
                statement.execute(createTableStatement);

                int numberOfTasks = settings.getNumberOfRows() / ROWS_PER_TASK;
                if (numberOfTasks == 0 || numberOfTasks == 1) {
                    tasks.add(new IngestTask(tableName, settings.getNumberOfRows(), settings.getNumberOfColumns()));
                } else {
                    int restRows = settings.getNumberOfRows() % ROWS_PER_TASK;
                    int restRowsPerTask = (restRows / numberOfTasks) + 1;

                    for (int j = 0; j < numberOfTasks; ++j) {
                        int currentRows = ROWS_PER_TASK;
                        if (restRows != 0) {
                            if (restRows > restRowsPerTask) {
                                currentRows += restRowsPerTask;
                                restRows -= restRowsPerTask;
                            } else {
                                currentRows += restRows;
                                restRows = 0;
                            }
                        }
                        tasks.add(new IngestTask(tableName, currentRows, settings.getNumberOfColumns()));
                    }
                }
            }
        }
        return tasks;
    }

    private static String buildCreateTableStatement(ApplicationSettings settings, String tableName) {
        StringJoiner createTableStatement = new StringJoiner(" ");
        createTableStatement.add("CREATE TABLE");
        createTableStatement.add(tableName);
        createTableStatement.add("(");
        createTableStatement.add("row0 INT");
        for (int j = 1; j < settings.getNumberOfColumns(); ++j) {
            createTableStatement.add(",row" + j + " INT");
        }
        createTableStatement.add(");");
        return createTableStatement.toString();
    }

    public static void main(String[] args) throws ParseException, ClassNotFoundException, SQLException {
        ApplicationSettings settings = ApplicationSettingsParser.parseCommandLineArguments(args);
        Class.forName(DRIVER_NAME);

        LOG.info("Application settings: " + settings);
        List<IngestTask> tasks = generateTasks(settings);
        tasks.forEach(LOG::info);
        Queue<IngestTask> taskQueue = new ConcurrentLinkedQueue<>(tasks);


        ExecutorService threadPool = Executors.newFixedThreadPool(settings.getNumberOfThreads());

        long begin = System.currentTimeMillis();
        for (int i = 0; i < settings.getNumberOfThreads(); ++i) {
            threadPool.submit(() -> {
                try (Connection connection = DriverManager.getConnection(
                        settings.getUri(),
                        settings.getUserName(),
                        settings.getPassword());
                     Statement statement = connection.createStatement()) {
                    if (settings.isBatchMode()) {
                        connection.setAutoCommit(false);
                    }

                    IngestTask task;
                    while ((task = taskQueue.poll()) != null) {
                        StringJoiner value = new StringJoiner(", ");
                        for (int j = 0; j < task.getNumberOfColumns(); ++j) {
                            value.add(Integer.toString(j));
                        }
                        String valueString = value.toString();
                        if (settings.isSingleInsert()) {
                            StringJoiner valueJoiner = new StringJoiner(", ");
                            for (int j = 0; j < task.getNumberOfRows(); ++j) {
                                valueJoiner.add("(" + valueString + ")");
                            }
                            statement.executeUpdate("INSERT INTO " + task.getTableName() + " VALUES " + valueJoiner.toString() + ";");
                        } else {
                            for (int j = 0; j < task.getNumberOfRows(); ++j) {
                                statement.executeUpdate("INSERT INTO " + task.getTableName() + " VALUES (" + valueString + ");");
                            }
                        }
                    }
                    if (settings.isBatchMode()) {
                        connection.commit();
                    }
                } catch (SQLException e) {
                    LOG.error(e);
                }
            });
        }
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            LOG.error("Too long", e);
        }
        long end = System.currentTimeMillis();
        long time = end - begin;
        LOG.info("Duration: " + time);

    }
}
